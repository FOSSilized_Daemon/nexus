# Table of Contents
* [Introduction](#Introduction)
	* [What is Nexus For PCs?](#what-is-nexus-for-pcs)
	* [Things to Note](#things-to-note)
	* [ Overview of ULOS](#overview-of-ulos)
* [Get Started](#get-started)
	* [Sections](#sections)
	* [Troubleshooting](#troubleshooting)

## Introduction
Welcome to Nexus For PCs! In this overview, one will be briefed on what this guide is and how to use it. Please consult the [Table of Contents](#table-of-contents) to navigate to a specific section, or read the next section to begin!

### What is Nexus For PCs?
Nexus For PCs is a guide detailing how to build a secure and privacy-focused operating system, targeting what are usually considered [personal computers](https://en.wikipedia.org/wiki/Personal_computer), named *ULOS*, or the **U**NIX and **L**isp-like **O**perating **S**ystem. This guide will go over everything from burning the ISO image one will use to configuring the userland using FOSSilized_Daemon's personal [dotfiles](https://www.gitlab.com/FOSSilized_Daemon/dotfiles).

If one has not read the [overview document for Nexus](README.md), please do so now. The overview document covers essential information, such as who should use this guide.

### Things to Note
Before one gets started, one must go over some essential items. Nexus can not cover every possible situation, nor is it likely to keep up with every advancement in technology; that being said, Nexus will be updated to meet those technological changes when possible. With these limitations in mind, Nexus will standardize a few things within this guide, and it will be on the user to adapt particular instructions to meet their specific needs. Whenever Nexus makes a standardization, as mentioned earlier, the user will be notified and reminded to make the needed changes to the commands provided.

### Overview of ULOS
The last thing that we will cover is the core concept of ULOS. ULOS is a modular and secure operating system inspired by modern UNIX-like operating systems and old Lisp machines. The goal is to create a programmable modular operating system that combines the versatility of the UNIX philosophy and common-lisp programmability. While ULOS is nowhere near that goal and is still in its infancy, it is still an exciting project for those wishing to create a minimalist operating system. The core of ULOS is not much, especially compared to other modern operating systems. Still, as time progresses, we intend to rewrite and implement drop-in replacements for much of the current operating system in common-lisp.

## Get Started
With all the formalities out of the way, it is time to get started! In this sub-section, one will find links to each section of this guide. Additionally, there will be a [Troubleshooting](#troubleshooting) sub-section, which will link to valuable notes and documentation to help users manage common issues such as the proper chroot steps.

### Sections
1. [Section 00: Configuring the Live Environment](sections/section-00_confguring-the-live-environment.md)
2. [Section 01: Configuring the System Drive](sections/section-01_configuring-the-system-drive.md)
3. [Section 02: Building the Base Operating System](sections/section-02_building-the-base-operating-system.md)

### Troubleshooting
