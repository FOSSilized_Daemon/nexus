# Table of Contents
* [Introduction](#introduction)
* [Burning the ISO Image](#burning-the-iso-image)
* [Booting the Live Environment](#booting-the-live-environment)
* [Setting Up the Network Connection](#setting-up-the-network-connection)
* [Installing Needed Programs Into Memory](#installing-needed-programs-into-memory)
* [Creating Needed Directories](#creating-needed-directories)
* [Fetching Useful Scripts and Configuring Files](#fetching-useful-scripts-and-configuration-files)
* [Exporting Needed Environment Variables](#exporting-needed-environment-variables)
* [Setting Up an SSH Connection (Optional)](#setting-up-an-ssh-connection-optional)

## Introduction
This section will cover how to build a live environment. Please consult the [Table of Contents](#table-of-contents) to navigate to a specific section, or read the following sub-section to begin!

### What is a Live Environment?
In this section, we will be configuring the *live environment* that we will use to build, configure, and install ULOS. Before getting to work, we must discuss what defines a live environment. Broadly speaking, a live environment is a pre-configured operating system that runs from memory instead of from a system drive. When creating our ULOS installation, we will be using our live environment to run commands, build software, and configure system files that will be installed on our system drive.

*Read more about what live environments are [here](https://en.wikipedia.org/wiki/Live_CD).*

### What Are the Steps to Configuring a Live Environment?
The steps to configuring a live environment vary depending on the operating system one installs and the method one uses to install it. For our purposes, the steps to configuring the live environment will be as described below.

1. [Burning the ISO Image](#burning-the-iso-image)
2. [Booting the Live Environment](#booting-the-live-environment)
3. [Setting Up the Network Connection](#setting-up-the-network-connection)
4. [Installing Needed Programs Into Memory](#installing-needed-programs-into-memory)
5. [Creating Needed Directories](#creating-needed-directories)
6. [Exporting Needed Environment Variables](#exporting-needed-environment-variables)
7. [Setting Up an SSH Connection (Optional)](#setting-up-an-ssh-connection-optional)

With that outlined, please proceed to the following sub-section: [Burning the ISO Image](#burning-the-iso-image).

## Burning the ISO Image
To begin configuring our live environment, we will first need to go through the process of *burning an ISO image to a USB flash drive*. An ISO image is a drive image containing all the information needed to run and install an operating system from memory. In order to use this ISO image, one will need to burn it to a device, such as a USB flash drive, and then boot it on one's computer. Burning an ISO image is relatively simple and mainly involves the following steps.

1. [Fetching the ISO Image](#fetching-the-iso-image)
2. [Verifying the ISO Signature](#verifying-the-iso-signature)
3. [Burning the ISO Image Onto the USB Flash Drive](#burning-the-iso-image-onto-the-usb-flash-drive)

Please continue to the first step: [Fetching the ISO Image](#fetching-the-iso-image).

*Read more about burning an ISO image [here](https://itstillworks.com/burn-iso-linux-6590522.html).*

### Fetching the ISO Image
The first thing we will need to do is fetch the ISO image of the operating system that we will be using for the live environment. Nexus for PCs officially uses the Arch Linux operating system for the live environment as it is small and easy to get working. In this step, we will be fetching the Arch Linux ISO image and its signature file to verify the ISO image's integrity. While there is a plurality of methods for fetching an ISO image, this guide supports the following two options.

* [Fetching the ISO Image Using the wget Command](#fetching-the-iso-image-using-the-wget-command)
* [Fetching the ISO Image Using a Web Browser](#fetching-the-iso-image-using-a-web-browser)

Please follow the correct instructions based on one's preference.

#### Fetching the ISO Image Using the wget Command
The `wget` command downloads files over the HTTP, HTTPS, and FTP protocols. Using the `wget` program, we can download the Arch Linux ISO image and its signature file. Before we fetch our files, it is recommended that users navigate to the [Arch Linux Download page](https://archlinux.org/download/) and select a close mirror, as this guide defaults to a worldwide one. Once one has chosen a mirror close to them, please run the following command. Be sure to substitute the provided links with the chosen links corresponding to one's selected mirror.
```
wget <url-of-iso> <url-of-iso-sig>
```
Some mirrors change the PGP signature of the ISO images they host, and due to this, one will be unable to properly verify the ISO image using the Arch Linux PGP keys. If one's mirror provides a *.key* file, make sure to download that as well; otherwise, one will be unable to verify the ISO image properly. However, one should note that any mirror that changes the signature of an ISO image is only as trustworthy as one feels it is. When an ISO image's signature is changed, it is often because it was tampered with and, therefore, should not be used under any circumstances.

Once those files finish downloading, one will have finished this step. Please continue onto the next sub-section: [Verifying the ISO Signature](#verifying-the-iso-signature).

#### Fetching the ISO Image Using a Web Browser
Some users may not use a UNIX-like operating system, have access to the `wget` command, or prefer a more graphical approach for downloading the needed files. We have added this sub-section with images detailing how to download the Arch Linux ISO image and its signature file using a web browser for these users. It should be noted that the below instructions should not be exactly copied. These instructions are written as a visual aid and do not reflect the current version of the ISO images provided by the Arch Linux development team.

First, navigate to the [Arch Linux Download page](https://archlinux.org/download/).

![Arch Linux Download Page](doc/guides/nexus-for-pcs/extras/images/arch-linux-download-page.png)

Next, scroll down and find one's country in the mirror list. Once one has found a mirror, click on its link and go to the mirror page.

![Mirror Page](doc/guides/nexus-for-pcs/extras/images/mirror-page.png)

Then, download the *.iso* and *.iso.sig* files. If one's mirror provides a *.key* file, make sure to download that as well; otherwise, one will be unable to verify the ISO image properly. However, one should note that any mirror that changes the signature of an ISO image is only as trustworthy as one feels it is. When an ISO image's signature is changed, it is often because it was tampered with and, therefore, should not be used under any circumstances.

Once those files finish downloading, one will have finished this step. Please continue onto the next sub-section: [Verifying the ISO Signature](#verifying-the-iso-signature).

### Verifying the ISO Signature
ISO image verification is crucial to burning a live USB flash drive. We verify the integrity of the ISO image to ensure that it has not been tampered with and potentially had malware installed on it. While there are many methods for verifying the integrity of an ISO image, this guide supports the following.

* [Verifying the ISO Signature Using GnuPG](#verifying-the-iso-signature-using-gnupg)

#### Verifying the ISO Signature Using GnuPG
The GnuPG program (or `gpg`/`gpg2`) is an open-source command-line utility for working with [PGP encryption](https://en.wikipedia.org/wiki/Pretty_Good_Privacy). As with Arch Linux, PGP encryption is often used to verify the integrity of ISO images. Using the GnuPG program, we can ascertain that our ISO image has not been tampered with and ensure that its signature is correct.

If the mirror one downloaded their ISO image from provided a *.key* file, then one should import it into their keyring. Otherwise, move on to the next command. Do note to replace the below *public.key* file name with one's respective file name.
```
gpg --import public.key
```
Now that we have everything downloaded and adequately set, such as importing the mirror public key if one was provided, we can verify our ISO image with the following command. Do note to replace the below ISO image and signature file name with the respective names of one's selected files.
```
gpg --keyserver-options auto-key-retrieve --verify archlinux-2021.02.01-x86_64.iso archlinux-2021.02.01-x86_64.iso.sig
```
If GnuPG complains about the keys not matching, then please consult the [insert-documentation-link](insert-documentation-link); otherwise, continue onto the next sub-section: [Burning the ISO Image Onto the USB Flash Drive](#burning-the-iso-image-onto-the-usb-flash-drive).

### Burning the ISO Image Onto a USB Flash Drive
Now that we have verified the integrity of our ISO image, it is finally time to burn it to a USB flash drive. There are several methods for burning an ISO image to a USB flash drive, but this guide only supports the following.

* [Burning the ISO Image Onto a USB Flash Drive Using the dd Command](#burning-the-iso-image-onto-the-usb-flash-drive-using-the-dd-command)

#### Burning the ISO Image Onto a USB Flash Drive Using the dd Command
The `dd` command is used to copy and covert data. The `dd` command is so powerful that it can be used to back up and restore an entire hard disk or disk partition. But, for our purposes, we will use the `dd` program to burn our Arch Linux ISO image to our USB flash drive.

Before we can burn our ISO image to our USB flash drive, we will need to determine which drive our USB flash drive is. Using the `lsblk` and `grep` programs, we can check to see if our operating system sees any drive other than our system drive. It should be noted that this guide will assume the [SDA partition scheme](https://www.tec4tric.com/linux/dev-sda-in-linux) when checking for USB flash drives; therefore, if one's USB flash drive is not detected as an SDA device, one will need to make adequate adjustments to all USB flash drive related commands.
```
lsblk | grep "sd"
```
The output returned should at least contain `/dev/sda`, with the last drive in the `sd` list being one's USB flash drive. If nothing more than `/dev/sda` is returned, then consult [insert-documentation-link](insert-documentation-link). Otherwise, if one sees their USB flash drive, we can continue using the `dd` command to burn our ISO image to it.
```
dd bs=4M if=<name-of-iso-image> of=/dev/sdb status=progress oflag=sync
```
Once the above command finishes we will have a bootable Arch Linux USB flash drive. Now we can move onto the next sub-section: [Booting the Live Environment](#booting-the-live-environment).

## Booting the Live Environment
Now that we have our bootable Arch Linux USB flash drive, it is finally time to boot into our live environment. The process of booting into our newly burnt live environment will take three steps.

1. [Plug In the USB Flash Drive into a USB Port](#plug-in-the-usb-flash-drive-into-a-usb-port)
2. [Enter the Computers BIOS](#enter-the-computers-bios)
3. [Select the USB Flash Drive from the "Bootable Media Menu"](#select-the-usb-flash-drive-from-the-bootable-media-menu)

### Plug In the USB Flash Drive into a USB Port
First, we must plug our USB flash drive into a USB port on our computer. Once we have plugged our USB flash drive into the computer, we can move on to the following sub-section: [Enter the Computers BIOS](#enter-the-computers-bios).

### Enter the Computers BIOS
Now that we have our USB flash drive plugged in, we can boot our computer and enter the BIOS. To enter our computer's BIOS, all we need to do is press the power button and then press the key that will enter us into the BIOS. The key we should press depends on the BIOS we are using, but it is usually *ESC*, *F1*, *F2*, *F8*, or *F10*. Once we have entered our computer's BIOS menu, we can move on to the following sub-section: [Select the USB Flash Drive from the "Bootable Media Menu"](#select-the-usb-flash-drive-from-the-bootable-media-menu).

### Select the USB Flash Drive from the "Bootable Media Menu"
This last step is very BIOS-dependent; therefore, one *may* need to consult one's respective BIOS documentation. The general rule of thumb is that once one has entered the BIOS, there should be a menu option called "Bootable Media." Select that menu option, select one's USB flash drive from the list of found media, and boot from it. Once one's computer has booted the USB flash drive, continue onto the following section: [Setting Up the Network Connection](#setting-up-the-network-connection).

## Setting Up the Network Connection
Now that we are booted into our live environment, it is time to start configuring it. We first want to set up a working network connection; luckily, Arch Linux comes with all the software we will need to do so quickly. Some users may be restricted to, or prefer, either an ethernet connection or a wireless connection. This guide offers the following sub-sections to accommodate both of these network connection types. Please follow the correct one corresponding to one's needs.

* [Setting Up a Wireless Network Connection](#setting-up-a-wireless-network-connection)
* [Setting Up a Wired Network Connection](#setting-up-a-wired-network-connection)

### Setting Up a Wireless Network Connection
Configuring a wireless network connection on Arch Linux is a trivial task thanks to programs like `wpa_supplicant`. This process will take four steps.

1. [Determining the Wireless Network Device](#determining-the-wireless-network-device)
2. [Setting Up the Wireless Network Device](#setting-up-the-wireless-network-device)
3. [Adding the WiFi Network](#adding-the-wifi-network)
4. [Connecting to the WiFi Network](#connecting-to-the-wifi-network)

With that outlined, proceed onto the first step: [Determining the Wireless Network Device](#determining-the-wireless-network-device).

#### Determining the Wireless Network Device
To connect to any network, we will need to determine what our wireless network device is. The easiest way to do this on Linux is using the `ip` command.
```
ip addr
```
From the output of this command, one should look for their wireless network device. Common names for wireless network devices are: *wlan0*, *wlan1*, *wlp7s0*, *wlp7s1*, *wlp5s0*, *wlp5s1*, etc. Once one has found their wireless network device, they should move on to the next step: [Setting Up the Wireless Network Device](#setting-up-the-wireless-network-device).

#### Setting Up the Wireless Network Device
Now that we know what our wireless network device is named, it is time to ensure it is set to "up." Using the following `ip` command, set the network device to "up." Use one's network device name in place of *wlan0*.
```
ip link set wlan0 up
```
After one has set their wireless network device to "up," move on to the next step: [Adding the WiFi Network](#adding-the-wifi-network).

#### Adding the WiFi Network
With our network device adequately configured, it is time to add our WiFi network's information to our system so we can connect. In the following command, we will add the network information for the WiFi network *"foo"* and its password *"bar"* to our system using the `wpa_passphrase` program. One should ensure to supplement the SSID, *"foo"*, and network password, *"bar"*, with their respective network's information.
```
wpa_passphrase 'foo bar' | tee /etc/wpa_supplicant/wpa_supplicant.conf
```
Proceed onto the next step: [Connecting to the Wifi Network](#connecting-to-the-wifi-network).

#### Connecting to the WiFi Network
Now that we have configured our wireless network device and added our network information to our system, it is time to connect to the internet. Using the following `wpa_supplicant` command, we can now connect to our WiFi network.
```
wpa_supplicant -B -c /etc/wpa_supplicant/wpa_supplicant.conf -i wlan0
```
To test that we are now connected to the internet, let us attempt to send 5 *ICMP* packets to *openbsd.org* using the `ping` program. If one's network uses IPv6 instead of IPv4, please replace the below `ping` command with `ping6`.
```
ping -c 5 openbsd.org
```
If the above command succeeds, then we now have our network connection. Move on to the next sub-section: [Installing Needed Programs Into Memory](#installing-needed-programs-into-memory).

### Setting Up a Wired Network Connection
Configuring a wired network connection on Arch Linux is a trivial task thanks to programs like `dhcpcd`. This process will take three steps.

1. [Determining the Wired Network Device](#determining-the-wireless-network-device)
2. [Setting Up the Wired Network Device](#setting-up-the-wireless-network-device)
4. [Connecting to the Wired Network](#connecting-to-the-wired-network)

With that outlined, proceed onto the first step: [Determining the Wired Network Device](#determining-the-wired-network-device).

#### Determining the Wired Network Device
To connect to any network, we will need to determine what our wired network device is. The easiest way to do this one Linux is using the `ip` command.
```
ip addr
```
From the output of this command, one should look for their wired network device. Common names for wired network devices are: *eth0*, *eth1*, *enp0s1*, *enp0s2*, etc. Once one has found their wired network device, they should move on to the next step: [Setting Up the Wired Network Device](#setting-up-the-wired-network-device).

#### Setting Up the Wired Network Device
Now that we know what our wired network device is named, it is time to ensure it is set to "up." Using the following `ip` command, put one's network device to "up," making sure to use one's network device name in place of *eth0*.
```
ip link set eth0 up
```
After one has set their wired network device to "up," move on to the next step: [Connecting to the Wired Network](#connecting-to-the-wired-network).

#### Connecting to the Wired Network
Now that we have configured our wired network device, it is time to connect to the internet. Using the following `dhcpcd` command, we can directly connect to our wired network.
```
dhcpcd eth0
```
To test that we are now connected to the internet, let us attempt to send 5 *ICMP* packets to *openbsd.org* using the `ping` program. If one's network uses IPv6 instead of IPv4, please replace the below `ping` command with `ping6`.
```
ping -c 5 openbsd.org
```
If the above command succeeds, then we now have our network connection. Move on to the next sub-section: [Installing Needed Programs Into Memory](#installing-needed-programs-into-memory).

## Installing Needed Programs Into Memory
Now that we have access to the internet, we can install all the programs we need into memory. When we install a program into memory, we install it into the live environment that we are using. One should note that any changes made to the live environment will **not persist across reboots**. This process takes two steps.

1. [Updating the Package Managers Database](#updating-the-package-managers-database)
2. [Installing the Needed Programs](#installing-needed-programs)

Installing our needed programs into memory will be a relatively quick and straightforward process, so without further ado, let us install our needed programs into memory.

### Updating the Package Managers Database
Package managers often maintain a local database to check against a foreign database to know what programs are available to be updated. Before we can install any programs, we will need to update the `pacman` database. `pacman` is the package manager that Arch Linux uses.
```
pacman -Sy
```
Once the database updates, move on to the next step: [Installing the Needed Programs](#installing-needed-programs).

### Installing the Needed Programs
We can install our needed programs now that we have an up-to-date `pacman` database. This *may* take a few minutes depending on one's internet speed.
```
pacman -Sy git wget gcc make sbcl
```
Once those programs have finished installing, move on to the following sub-section: [Creating Needed Directories](#creating-needed-directories).

## Creating Needed Directories
As we will be building a lot of software from source and generally doing many different tasks to make ULOS, creating some directories is an excellent idea to keep everything in order. Let us go ahead and create a root directory to be used as our target ULOS system. Then, we need to create a script directory to store helpful scripts. Next, let us create a configuration directory to store pre-made configuration files. Finally, we will create a builds directory where we will be building software. These directories should be stored within the live environments `HOME` directory for simplicity's sake.
```
mkdir -p ~/ulos-root
mkdir -p ~/ulos-scripts
mkdir -p ~/ulos-configs
mkdir -p ~/ulos-build
```
Fantastic, now move on to the next sub-section: [Fetching Useful Scripts and Configuration Files](#fetching-useful-scripts-and-configuration-files).

## Fetching Useful Scripts and Configuration Files
During the process of building ULOS, there will be **a lot** of mundane tasks that we will need to do. To make doing all of these mundane tasks as simple and streamlined as possible, we will need to pull some pre-made configuration files and scripts to help us automate things. Using the `git` program, let us clone our pre-made configuration files into the `~/ulos-configs` directory and then clone our scripts into the `~/ulos-scripts` directory.
```
git clone https://gitlab.com/FOSSilized_Daemon/nexus-scripts.git ~/dulos-scripts
git clone https://gitlab.com/FOSSilized_Daemon/nexus-configs.git ~/dulos-configs
```
Now move on to the next sub-section: [Exporting Needed Environment Variables](#exporting-needed-environment-variables).

## Exporting Needed Environment Variables
Within the UNIX world, there is an exceptional concept known as an *environment variable*. An environment variable is a dynamically named value that is stored in memory. These variables are instrumental when writing everything from simple shell scripts to complex programs and are even used within UNIX kernels such as the Linux kernel. Below we will export three environment variables that will allow us to type less as we build ULOS.
```
export ULOS_ROOT="${HOME}"/ulos-root
export ULOS_SCRIPTS="${HOME}"/ulos-scripts/src/DULOS/
export ULOS_CONFIGS="${HOME}"/ulos-configs/src/DULOS/
export ULOS_BUILD="${HOME}"/ulos-build
```
With that done, we are ready to start configuring our system drive. Those who wish to set up an SSH connection for the rest of this guide may move on to the following sub-section: [Setting Up an SSH Connection (Optional)](#setting-up-an-ssh-connection-optional). Those not interested in setting up an SSH connection may move on to the next section: [Configuring the System Drive](section-01_configuring-the-system-drive.md).

## Setting Up an SSH Connection (Optional)
The *secure shell*, or SSH, is a widespread network protocol that gives users a safe way to access a computer remotely. While there are a plethora of use cases for SSH, in this sub-section, we will cover how to set up and SSH into our target machine. This sub-section will take three steps.

1. [Setting the Root Password](#setting-the-root-password)
2. [Enabling the SSH Daemon](#enabling-the-ssh-daemon)
3. [SSHing Into the Live Environment](#sshing-into-the-live-environment)

Move on to the following sub-section to get started: [Setting the Root Password](#setting-the-root-password).

### Setting the Root Password
To log into our target machine remotely using SSH, we will need an account and password that we can use; for this, the root account will suffice. Using the `passwd` command, we can set the root account password to the password we will use to log in via SSH.
```
passwd
```
With that done, move on to the next sub-section: [Enabling the SSH Daemon](#enabling-the-ssh-daemon).

### Enabling the SSH Daemon
We will need SSH running on our target machine to SSH into it; while there are many methods for running SSH in the background on Arch Linux, we can enable the SSH daemon using `systemctl`.
```
systemctl enable ssh
systemctl start ssh
```
Time to finally go our main machine and SSH into the target machine, move onto the following sub-section: [SSHing Into the Live Environment](#sshing-into-the-live-environment).

### SSHing Into the Live Environment
Before we can SSH into the target machine, we will need to figure out its IP address. Determining the IP address of our target machine is a simple task using the `ip` command.
```
ip addr
```
Look through the output of the above command for a string like `192.0.2.146` or `2607:f0d0:1002:51::4`. Once one has found their IP address, they should use SSH to connect to their target machine from their primary machine remotely. One should note that the IPv4 address used below is just an example, not the IP address one should use in the following command.
```
ssh root@192.0.2.146
```
Fantastic, we have fully configured the live environment and are ready to move on to configuring our system drive. Move on to the next section: [Configuring the System Drive](section-01_configuring-the-system-drive.md).
