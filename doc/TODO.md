# Scripts
- [] Write a script to build a proper file system hierarchy.

# README
- [] Add all useful privacy resources.
- [] Add all useful hardware resources.
- [] Fix any technical mistakes.
- [] Fix any spelling and grammar errors.

# Nexus for PCs Introduction
- [] Add all documentation links.
- [] Add all section links.
- [] Add troubleshooting links.
- [] Fix any technical mistakes.
- [] Fix any spelling and grammar errors.

# Configuring the Live Environment
- [] Add all documentation links.
- [] Add the ISO verification section.
- [] Add the Rufus section.
- [] Add the Booting the Live Environment section.
- [] Fix any technical mistakes.
- [] Fix any spelling and grammar errors.

# Building the Base OS
- [] Add steps for building the C and C++ toolchain.
- [] Add steps for building the common lisp toolchain.
- [] Add steps for building the guile lisp toolchain.
- [] Add steps for building the POSIX core utilities.
- [] Add steps for building the GNU Guix package manager (ensure guix uses the proper directories (~/.local/bin, ~/.local/lib, ~/.local/etc, ~/.local/lib/include, ~/.local/lib/libexec) for storage.

# Configuring System Files
- [] Add steps for configuring /etc/fstab
- [] Add steps for configuring /etc/passwd
- [] Add steps for configuring /etc/group
- [] Add steps for configuring /etc/shells
- [] Add steps for configuring /etc/profile
- [] Add steps for configuring /etc/hostname
- [] Add steps for configuring /etc/issue

# Style Guide
- [] Add explanation of file and directory naming conventions.
